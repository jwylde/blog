#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/stat.h>

/*
#include <openssl/aes.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/buffer.h>
*/

int main(int argc, char * argv[])
{
	int testmode = 1; /* 1 for testmode, 0 for regular mode */
		if (testmode)
	{	
		printf ("%d/n", argc);
		puts("Hello World");
		for (int i = 0; i < argc; i++)
		{
			puts(argv[i]);
		}
	}
	
   if (argc < 5)
   {
		puts("invalid");  
		if (testmode) puts("not enough arguments");
      return(255);
   }   

  	
	/* This is the first loop through the argument list */
	/* This scans the argument list for options -K  and -I and counts names and obtains secret key*/
	/* Aso this section makes sure only one of -I, -S, -R, or -T is set */
	int optionI =0;   /* are we using option -I */
	int nnames = 0;   /* counts the number of names for option -I */
	char * secret;    /* value of secret key */
	int kset = 0;     /* Set to 1 if option -K exists */
	char * tempstr;	/* a temprary string pointer */
	char xset = ' ';  /* which of -I, -S, -R, or -T is set */
	char eorg = ' ';  /* set to 'E' if a "-E" option found set to 'G' if a "-G" option found */
	int eorgdup = 0;  /* set to 1 if both -E and -G are found */
	char * argvi;		/* argvi is set to argv[i] for convenience */
	int totargs = argc - 1;/* total valid args */
	char nametype;    /* for options R and T , is set to E or G */
	 		
	for (int i = 1; i < argc; i++)
   {
      argvi = argv[i];
	   		
		/* check argument -I ======================================================= */		
		if (!strncmp(argvi,"-I",2))
      {
			if (xset!=' ' && xset != 'I')
			{	
				puts("invalid"); 
				if (testmode) {puts("more than one of -I, -S, -R, or -T is set");}
				return(255);
			}
			if (xset != ' ') totargs--; /*extra arg of same type */
			if (argvi[2] != '\0')
			{	
				puts("invalid");  
				if (testmode) puts("fake -I");
				return(255);
			}
			xset = 'I';			
			optionI =1;
			continue;
      }
		
		/* check argument -K ======================================================= */
		if (!strncmp(argv[i],"-K",2))
      {
			if (i == argc-1)
			{	
				puts("invalid"); 
				if (testmode) puts("option -K can't be last option");
				return(255);
			}
			tempstr = argv[i+1];
			if ( tempstr[0] == '-'    )
			{	
				puts("invalid"); 
				if (testmode) puts("option follwing -K must not start with - ");
				return(255);
			}
			if (argvi[2] != '\0')
			{	
				puts("invalid");  
				if (testmode) puts("fake -K");
				return(255);
			}
			if (kset == 1) totargs--; /*extra arg of same type */
			kset = 1;
			secret = argv[i+1];
			continue;
      }
		
		/* check arguments -E -G ======================================================= */
		/* count the number of potential guests and employees ============================= */
		if (!strncmp(argv[i],"-E",2) || !strncmp(argv[i],"-G",2))
      {
			if (i == argc-1)
			{	
				puts("invalid");  
				if (testmode) puts("options -E and -G can't be last option");
				return(255);
			}
			tempstr = argv[i+1];
			
			if ( tempstr[0] == '-') 
			{	
				puts("invalid");  
				if (testmode) puts("option follwing -E and -G must not start with -");
				return(255);
			}
			int j = 0;
			while (tempstr[j]!='\0')
			{
				if (!isalpha(tempstr[j]))
				{
					puts("invalid");  
					if (testmode) puts("argument following -E and -G is not alpha");
					return(255);
				}
				j++;
			}	
				 
			
			if (argvi[2] != '\0')
			{	
				puts("invalid"); 
				if (testmode) puts("fake -E or -G");
				return(255);
			}
			if ( tempstr[1] != eorg && eorg != ' ' )
			{	
				eorgdup = 1;
			}
			else
			{
				eorg = tempstr[1];
			}
			nnames++;
			nametype = (argv[i])[0];
			continue;
      }
		
		/* check arguments -S -T -R */
		if (!strncmp(argv[i],"-S",2) || !strncmp(argv[i],"-T",2) || !strncmp(argv[i],"-R",2) )
		{
			tempstr = argv[i];
			if (xset!=' ' && xset != tempstr[1])
			{	
				puts("invalid");  
				if (testmode) puts("more than 1 of -S -T -R -I found");
				return(255);
			}
			if (xset != ' ') totargs--; /*extra arg of same type */
			if (argvi[2] != '\0')
			{	
				puts("invalid");  
				if (testmode) puts("fake -S, -T, or - R");
				return(255);
			}
			xset = tempstr[1];
			continue;
		}		
		
   } /* end of first loop through arguments */
	
	if (testmode)printf("nnames = %d\n", nnames);
	
	/*========================================================================*/
	
	/* Check the parameters so far to see if we are still valid */
	if (!kset)
	{
		puts("invalid");    
		if (testmode) puts("no -K option specified");
		return(255);
	}
	if (xset == ' ')
	{
		puts("invalid");    
		if (testmode) puts("No report type specified");
		return(255);
	}		
	if (eorgdup && xset != 'I')
	{	
		puts("invalid");    
		if (testmode) puts("both an e and g appeared and option was not -I");
		return(255);
	}
	if (eorg != ' ' && xset == 'S')
	{	
		puts("invalid");    
		if (testmode) puts("one of e or g appeared and option was -S");
		return(255);
	}
	if (nnames != 1 && (xset == 'R' || xset =='T') )
	{	
		puts("invalid");    
		if (testmode) puts("0 or more than 1 of -E or -G appeared and option was -R or -T ");
		return(255);
	}
	/*========================================================================*/
	
   /* This is the second loop through the argument list */
	/* Finds employee and guest name (-E or -G) or names (-I) */
	int nameindex = 0;
	char ** inames = malloc(nnames*sizeof(char*)); /* names for option -I */
	if (xset != 'S')
	{
	for (int i = 1; i < argc; i++)
		{
			tempstr = argv[i];
			if( strlen(tempstr) ==2 && tempstr[0] == '-' && (tempstr[1] == 'E' || tempstr[1] == 'G')) 
			{
				inames[nameindex] = argv[i+1];
				nameindex++;
			}
		} /* end of second loop through arguments */
	} /* end of for (int i = 1; i < argc; i++) */
   /*========================================================================*/
	
	/* some final checks */
	if (xset == 'S' && totargs != 4)
	{	
		puts("invalid");    
		if (testmode) puts("total number of arguments wrong for option S");
		return(255);	
	}	
	if ( (xset == 'R' || xset == 'T') && totargs != 6 )
	{	
		puts("invalid");    
		if (testmode) puts("total number of arguments wrong for option R or T");
		return(255);	
	}	
	if (xset == 'I')
	{
		if (totargs != 4 + 2*nnames)
		{	
			puts("invalid");    
			if (testmode) puts("Wrong number of arguments for option I");
			return(255);	
		}	
	}
	/*========================================================================*/	
	
	/* Print results so far */
	
	if (testmode)
	{	
		printf("xset=|%c|\n", xset);
		if (xset == 'I')
		{
			printf("nnames=|%d|\n", nnames);
			for (int i = 0; i < nnames; i++)
			{
				printf("inames[%d]=|%s|\n", i, inames[i]);
			}			
		}	
		if (xset == 'T' || xset == 'R')
		{
			printf("nnames=|%d|\n", nnames);
			printf("nametype=|%c|\n", nametype);
			for (int i = 0; i < nnames; i++)
			{
				printf("inames[%d]=|%s|\n", i, inames[i]);
			}			
		}		
	}
	/*========================================================================*/

	/*read char**data*/
	char ** data = malloc(5*sizeof(char*));
	data[0] = "Log2 Secret 34 1";
	data[1] = "E Sam 3 25 A -1 26 A 23 34 L 23";
	data[2] = "A 3 26 L 3 22 A 3 21 A -1";
	data[3] = "A 2 5 A -1";
	data[4] = "L 3 1 A 3 0 A -1";

   char * firstline = data[0];

	/*========================================================================*/
	
	/* Write reports */
	if (xset == 'R' || xset =='T') 
	{
		
	}		


free(inames);	
   
}

